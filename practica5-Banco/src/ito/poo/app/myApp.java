package ito.poo.app;
import java.time.LocalDate;
import ito.poo.clases.CuentaBancaria;
public class myApp {
	
	public static void run() {
		CuentaBancaria c = new CuentaBancaria(4897683L,"Raul Lopez",70000F,LocalDate.of(2021, 3, 21));
		System.out.println(c);
		System.out.println(c.Retiro(25000F, LocalDate.of(2021, 4, 22)));
		System.out.println(c.Deposito(30000F, LocalDate.of(2021, 6, 22)));
		System.out.println(c);
		CuentaBancaria c1 = new CuentaBancaria(17855645L,"yamil Reyes",30000F,null);
		System.out.println(c1);
		System.out.println(c1.Retiro(65000F, LocalDate.of(2021, 7, 22)));
		System.out.println(c1.Deposito(20000F, LocalDate.of(2021, 8, 22)));	
		CuentaBancaria c2 = new CuentaBancaria(17855645L,"Arely Barrales",70000F,LocalDate.of(2021, 8, 22));
		System.out.println(c2);
		System.out.println(c2.Retiro(25000F, LocalDate.of(2022, 9, 22)));
		System.out.println(c2.Deposito(30000F, LocalDate.of(2022, 9, 22)));
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		run();

	}

}
