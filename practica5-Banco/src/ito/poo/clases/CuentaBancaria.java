package ito.poo.clases;
import java.time.LocalDate;
public class CuentaBancaria {
	
	private long numeroCuenta = 0L;
	private String nombreCliente = "";
	private float saldo = 0F;
	private LocalDate fechaApertura = null;
	private LocalDate fechaActualizacion = null;

	
	public CuentaBancaria() {
		super();
	}
	public CuentaBancaria(long numCuenta, String nombCliente, float saldo, LocalDate fechaApertura) {
		super();
		this.numeroCuenta = numCuenta;
		this.nombreCliente = nombCliente;
		this.saldo = saldo;
		this.fechaApertura = fechaApertura;
	}

	public boolean Deposito(float Cantidad,LocalDate newfechaActualizacion) {
		boolean Deposito = false;
		if(this.fechaApertura==null)
			System.out.println("La cuenta no se encuentra activa");
		else {
			Deposito = true;
			this.setSaldo(this.getSaldo()+ Cantidad);
			this.setFechaActualizacion(newfechaActualizacion);
		}
		return Deposito;
	}

	public boolean Retiro (float Cantidad,LocalDate newFechaActualizacion) {
		boolean Retiro = false;
		if (Cantidad <= this.getSaldo()) {
			 Retiro=true;
			 this.setSaldo(this.getSaldo()-Cantidad);
			 this.setFechaActualizacion(newFechaActualizacion);
			}
		else
			System.out.println("Fondos insuficientes");
		return Retiro;
	}

	public long getNumCuenta() {
		return numeroCuenta;
	}

	public void setNumCuenta(long numCuenta) {
		this.numeroCuenta = numCuenta;
	}

	public String getNomCliente() {
		return nombreCliente;
	}

	public float getSaldo() {
		return saldo;
	}

	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}

	public LocalDate getFechaApertura() {
		return fechaApertura;
	}

	public void setFechaApertura(LocalDate fechaApertura) {
		this.fechaApertura = fechaApertura;
	}

	public LocalDate getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(LocalDate fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	@Override
	public String toString() {
		return "CuentaBancaria [numCuenta=" + numeroCuenta + ", nomCliente=" + nombreCliente + ", saldo=" + saldo
				+ ", fechaApertura=" + fechaApertura + ", fechaActualizacion=" + fechaActualizacion + "]";
	}
}
